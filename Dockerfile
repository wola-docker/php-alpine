FROM php:8.0-cli-alpine

RUN apk update && apk add  --no-cache \
    curl \
    openssh-client \
    git \
    autoconf \
    g++ \
    make \
    postgresql-dev \
    libpng-dev \
    libjpeg-turbo-dev \
    libpng \
    libwebp-dev \
    zlib-dev \
    libxpm-dev \
    freetype \
    freetype-dev \
    libffi-dev \
    libzip-dev \
    libc6-compat \
    zip &&\
    curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer &&\
    pecl install -o -f redis &&  rm -rf /tmp/pear && \
    docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install ffi pdo_pgsql gd zip pcntl sockets && \
    docker-php-ext-enable redis && \
    mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
